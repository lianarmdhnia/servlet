<%@ page import ="java.sql.*" %>
<%@ page import ="java.util.*" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Dashboard</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

h2{
    margin-top: 20px;
    margin-bottom: 10px;
  }

tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body>
    <div class="container" >
    <h2>Daftar Mahasiswa</h2>
    <a class="btn btn-primary" href="/tugas/insert" style="margin-bottom:30px; text-align:right;">Tambah data</a>
        <div style="overflow-x:auto;">
          <table>
            <tr>
              <th>No</th>
              <th>FullName</th>
              <th>Address</th>
              <th>Status</th>
              <th>Grade Physics</th>
              <th>Grade Calculus</th>
              <th>Grade Biologi</th>
              <th>Action</th>
            </tr>
                    <%
                        ResultSet user = (ResultSet) request.getAttribute("users");
                        int i =1;
                    %>

                    <% while(user.next()){ %>
                           <tr>
                                   <td><%=i++%></td>
                                   <td><%=user.getString("fullname")%></td>
                                   <td><%=user.getString("address")%></td>
                                   <td><%=user.getString("status")%></td>
                                   <td><%=user.getString("gradesphysics")%></td>
                                   <td><%=user.getString("gradescalculus")%></td>
                                   <td><%=user.getString("gradesbiologi")%></td>
                                   <td><a href="EditMahasiswa?id=<%=user.getString("id")%>">edit</a>
                                   <form method="post" action="DeleteMahasiswa?id=<%=user.getString("id")%>"><button type="submit"
                                   onclick="return confirm('apakah anda yakin ingin menghapus data ini?')">delete</button</form></td>
                            </tr>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</body>
</html>